=====
Use Case:
Game turn
=====
Actor:
Players
=====
Precondition:
<list of preconditions>
=====
Postcondition:
<list of postconditions>
=====
Trigger:
 * Start game
 * End turn completed
=====
Main path:
 * Bidding phase
 * Initiative phase
 * Events phase
 * Hyperjumps phase
 * Opportunities phase
 * Investment phase
 * Control phase
=====
